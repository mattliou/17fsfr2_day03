// Load express library
var express = require('express')

// create an instance of the express application
var app = express();

// configure the routes
// check reand match request

app.use(express.static(__dirname + "/Public"));
// create my server
// specify the port that the app will be listening to
var port = 3000;

// bind the app to the port
app.listen(port, function() { 
    console.log('Application started on port %d', port)
});